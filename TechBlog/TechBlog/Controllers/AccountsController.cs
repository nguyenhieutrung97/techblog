﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using TechBlog.Models.EFModels;
using TechBlog.Models.ViewModels;
using TechBlog.Services;

namespace TechBlog.Controllers
{
    public class AccountsController : Controller
    {
        private readonly IAccountService _accountService;
        public AccountsController(IAccountService accountService)
        {
            _accountService = accountService;
        }
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login([Bind("Email,Password")]LoginVM loginVM)
        {
            if (ModelState.IsValid)
            {
                var loginResult = await _accountService.Login(loginVM);
                if (loginResult != null)
                {
                    var identity = new ClaimsIdentity(new[] { new Claim(ClaimTypes.Name, loginResult.NameDisplay), new Claim(ClaimTypes.NameIdentifier, loginResult.Id.ToString()) }, CookieAuthenticationDefaults.AuthenticationScheme);

                    var principal = new ClaimsPrincipal(identity);

                    var login = HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, principal);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "Username or Password not match!!!");
                    return View(loginVM);
                }
            }
            return View(loginVM);
        }
        public IActionResult Logout()
        {
            var login = HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return RedirectToAction("Login", "Accounts");
        }
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register([Bind("Id,Password,Email,NameDisplay,ImageUrl")] User user)
        {
            if (ModelState.IsValid)
            {
                RegisterState registerResult = await _accountService.Register(user);
                if (registerResult == RegisterState.EmailExist)
                {
                    ModelState.AddModelError("", "Email Exist!!!");
                    return View(user);
                }
                else if (registerResult == RegisterState.UserNameExist)
                {
                    ModelState.AddModelError("", "Username Exist!!!");
                    return View(user);
                }
                else
                {
                    return RedirectToAction("RegisterResult", "Information", new { result = true });
                }
            }
            return View(user);
        }
    }
}