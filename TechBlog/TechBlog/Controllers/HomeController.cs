﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TechBlog.Data;
using TechBlog.Services;

namespace TechBlog.Controllers
{
    public class HomeController : Controller
    {
        private readonly IMock _mock;
        private readonly IBlogService _blogService;

        public HomeController(IMock mock, IBlogService blogService)
        {
            _mock = mock;
            _blogService = blogService;
        }
        [Route("blogs/{tagName?}")]
        [Route("")]
        [Route("Home")]
        [Route("Home/Index")]
        [Route("Home/Index/{id?}")]
        public async Task<IActionResult> Index(string tagName)
        {
            await _mock.FakeData();
            if (tagName != null)
            {
                return View(await _blogService.GetByTagName(tagName));
            }
            return View(await _blogService.GetAll());
        }
        [Route("privacy")]
        public IActionResult Privacy()
        {
            return View();
        }

    }
}
