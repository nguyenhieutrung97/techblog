﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TechBlog.Data;
using TechBlog.Models;
using TechBlog.Models.EFModels;
using TechBlog.Models.ViewModels;
using TechBlog.Services;

namespace TechBlog.Controllers
{
    public class BlogsController : Controller
    {
        private Guid userIdFake = new Guid("DEEED523-E376-4FF0-8439-06381DA087CE");
        private readonly IBlogService _blogService;
        private readonly IBlogDetailService _blogDetailService;
        private readonly IModelService<Tag> _tagService;
        public BlogsController(IBlogService blogService, IBlogDetailService blogDetailService, IModelService<Tag> tagService)
        {
            _blogService = blogService;
            _blogDetailService = blogDetailService;
            _tagService = tagService;
        }
        
        [Authorize]
        public async Task<IActionResult> Create()
        {
            var tags = await _tagService.GetAll();
            ViewBag.SelectTags = tags.Select(t => new SelectListItem { Value = t.Id.ToString(), Text = t.Name });
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Title,HtmlContent,ImageDisplay,Tags")] BlogPostVM blogPostVM)
        {
            var tags = await _tagService.GetAll();
            ViewBag.SelectTags = tags.Select(t => new SelectListItem { Value = t.Id.ToString(), Text = t.Name });
            if (ModelState.IsValid)
            {
                var idCurrentUser = new Guid(User.FindFirst(claim => claim.Type == System.Security.Claims.ClaimTypes.NameIdentifier)?.Value);
                if (await _blogService.Create(idCurrentUser, blogPostVM))
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    ModelState.AddModelError("", "Create blog failed!!!");
                    return View(blogPostVM);
                }
            }
            return View(blogPostVM);
        }

        public async Task<IActionResult> Detail(Guid? id)
        {
            if (!id.HasValue)
            {
                return NotFound("Product Id Not Found");
            }

            return View(await _blogDetailService.GetByBlogId(id.GetValueOrDefault()));
        }
    }
}