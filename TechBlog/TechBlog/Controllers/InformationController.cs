﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace TechBlog.Controllers
{
    public class InformationController : Controller
    {
        public IActionResult RegisterResult(bool result)
        {
            ViewBag.Result = result;
            return View();
        }
    }
}