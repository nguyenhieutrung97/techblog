﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TechBlog.Models.ViewModels
{
    public class BlogPostVM
    {
        [Required]
        public string Title { get; set; }
        [Required]
        [MinLength(20)]
        public string HtmlContent { get; set; }
        [Required]
        public List<string> Tags { get; set; }
        [Required]
        public IFormFile ImageDisplay { get; set; }
    }
}
