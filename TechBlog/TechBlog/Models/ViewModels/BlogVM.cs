﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechBlog.Models.EFModels;

namespace TechBlog.Models.ViewModels
{
    public class BlogVM
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public DateTime CreatedTime { get; set; }
        public string ImageUrl { get; set; }
        public string NameAuthor { get; set; }
        public string ImageAuthorUrl { get; set; }
        public List<Tag> Tags { get; set; }
    }
}
