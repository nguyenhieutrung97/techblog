﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TechBlog.Models.EFModels;

namespace TechBlog.Models.ViewModels
{
    public class BlogDetailVM
    {
        public string Title { get; set; }
        public string ImageUrl { get; set; }
        public string HtmlContent { get; set; }
        public List<Tag> Tags { get; set; }
        public DateTime CreatedTime { get; set; }
        public string NameAuthor { get; set; }
        public string EmailAuthor { get; set; }
        public string ImageAuthorUrl { get; set; }
    }
}
