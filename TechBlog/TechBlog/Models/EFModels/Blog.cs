﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TechBlog.Models.EFModels
{
    public class Blog
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(200)]
        public string Title { get; set; }
        [Required]
        [StringLength(200)]
        public string ImageUrl { get; set; }
        [Required]
        public DateTime CreatedTime { get; set; }

        public Guid UserId { get; set; }
        public User User { get; set; }
        public ICollection<BlogTag> BlogTags { get; set; }
    }

}
