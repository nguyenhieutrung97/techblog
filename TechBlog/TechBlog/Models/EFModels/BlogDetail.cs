﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TechBlog.Models.EFModels
{
    public class BlogDetail
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [Column(TypeName = "ntext")]
        public string HtmlContent { get; set; }

        public Guid BlogId { get; set; }
        public Blog Blog { get; set; }
    }
}
