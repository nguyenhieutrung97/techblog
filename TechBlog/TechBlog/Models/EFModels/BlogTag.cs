﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TechBlog.Models.EFModels
{
    public class BlogTag
    {
        [Required]
        public Guid BlogId { get; set; }
        public Blog Blog { get; set; }
        [Required]
        public Guid TagId { get; set; }
        public Tag Tag { get; set; }
    }
}
