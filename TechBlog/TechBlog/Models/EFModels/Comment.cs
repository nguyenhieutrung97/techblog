﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TechBlog.Models.EFModels
{
    public class Comment
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(200)]
        public string CommentContent { get; set; }
        [Required]
        public DateTime CreatedTime { get; set; }

        public Guid UserId { get; set; }
        public Guid BlogDetailId { get; set; }
        public User User { get; set; }
    }
}
