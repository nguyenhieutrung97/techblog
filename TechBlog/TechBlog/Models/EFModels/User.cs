﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TechBlog.Models.EFModels
{
    public class User
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        [StringLength(30)]
        public string Email { get; set; }
        [Required]
        [StringLength(40)]
        public string NameDisplay { get; set; }
        [StringLength(200)]
        public string ImageUrl { get; set; }
        [Required]
        public DateTime CreatedTime { get; set; }

        public Guid RoleId { get; set; }
        public Role Role { get; set; }
        public ICollection<Blog> Blogs { get; set; }
    }
}
