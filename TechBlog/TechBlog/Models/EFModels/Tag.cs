﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TechBlog.Models.EFModels
{
    public class Tag
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        [StringLength(40)]
        public string Name { get; set; }

        public ICollection<BlogTag> BlogTags { get; set; }
    }
}
