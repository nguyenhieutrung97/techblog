﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechBlog.Extensions;
using TechBlog.Models;
using TechBlog.Models.EFModels;
using TechBlog.Services;

namespace TechBlog.Data
{
    public interface IMock
    {
        Task FakeData();
    }
    public class Mock : IMock
    {
        private readonly TechBlogContext _context;
        private readonly IHtmlService _htmlService;
        public Mock(TechBlogContext context, IHtmlService htmlService)
        {
            _htmlService = htmlService;
            _context = context;
        }
        public async Task FakeData()
        {
            if (_context.Users.Any() == false)
            {
                List<Role> roles = new List<Role>();
                Role role1 = new Role { Id = new Guid("3ffcabba-a1b2-4fa0-a928-483303246ab9"), Name = "Admin" };
                Role role2 = new Role { Id = new Guid("daa3481e-e333-4add-aa3e-df35af99ef87"), Name = "User" };
                await _context.Roles.AddAsync(role1);
                await _context.Roles.AddAsync(role2);
                await _context.SaveChangesAsync();
                List<User> users = new List<User>();
                User user1 = new User { Id = new Guid("DEEED523-E376-4FF0-8439-06381DA087CE"), NameDisplay = "Trung Bamboo", Password = "f23f5983b54d69ff22c22661ff764de82beef64ec25137be5be1acbfb98bcf8a", Email = "nguyenhieutrung@gmail.com", ImageUrl = "/images/user/deeed523-e376-4ff0-8439-06381da087ce/avatar.jpg", RoleId = new Guid("3ffcabba-a1b2-4fa0-a928-483303246ab9") };
                User user2 = new User { Id = new Guid("3838C61D-54DE-419B-A383-A0D8C6336D3A"), Email = "truongtankhanh@gmail.com", NameDisplay = "Khánh IT", Password = "962bbf015cabdde1f686f29539b893ea02b2cb6cff78de275bf8c6821827b9c5", ImageUrl = "/images/user/3838c61d-54de-419b-a383-a0d8c6336d3a/avatar.jpg", RoleId = new Guid("daa3481e-e333-4add-aa3e-df35af99ef87") };
                await _context.Users.AddAsync(user1);
                await _context.Users.AddAsync(user2);
                List<Tag> tags = new List<Tag>();
                Tag tag1 = new Tag { Id = new Guid("692AF7F8-8440-424A-A1EA-051C3E69E88D"), Name = "css" };
                Tag tag2 = new Tag { Id = new Guid("77091F52-28D0-4C8F-8CBA-248026707E8A"), Name = "beginners" };
                Tag tag3 = new Tag { Id = new Guid("D3E04309-565C-4473-B7DD-59C3E3F3827B"), Name = "javascript" };
                Tag tag4 = new Tag { Id = new Guid("74A34FFA-CDD5-4496-941E-5B03D4E4C674"), Name = "webdev " };
                Tag tag5 = new Tag { Id = new Guid("FA392B70-5EF0-487A-98E6-9A51314FE252"), Name = "node" };
                Tag tag6 = new Tag { Id = new Guid("886961D3-CF19-4979-815C-C1EE600F2063"), Name = "react" };
                Tag tag7 = new Tag { Id = new Guid("19A08140-724B-4572-B5EA-E781154983F9"), Name = "html" };
                Tag tag8 = new Tag { Id = new Guid("ccf6a9dd-050b-4228-a92f-706ba8d28cd9"), Name = "dotnet" };
                Tag tag9 = new Tag { Id = new Guid("a4281f8f-8211-4721-9acf-6508e1e9fb54"), Name = "csharp" };
                await _context.Tags.AddAsync(tag1);
                await _context.Tags.AddAsync(tag2);
                await _context.Tags.AddAsync(tag3);
                await _context.Tags.AddAsync(tag4);
                await _context.Tags.AddAsync(tag5);
                await _context.Tags.AddAsync(tag6);
                await _context.Tags.AddAsync(tag7);
                await _context.Tags.AddAsync(tag8);
                await _context.Tags.AddAsync(tag9);
                await _context.SaveChangesAsync();
                Blog blog1 = new Blog { Id = new Guid("1eb95ad2-6b8c-4abc-a716-8cbdb56c70fe"), Title = "Prototype Design Pattern in C#", ImageUrl = "/images/blog/1eb95ad2-6b8c-4abc-a716-8cbdb56c70fe/blog_display.jpg", UserId = new Guid("deeed523-e376-4ff0-8439-06381da087ce"), CreatedTime = System.DateTime.Now };
                Blog blog2 = new Blog { Id = new Guid("44a731c0-6c0c-4b29-aee3-b3ddbb651496"), Title = "DotVVM Authorization with IdentityServer4", ImageUrl = "/images/blog/44a731c0-6c0c-4b29-aee3-b3ddbb651496/blog_display.jpg", UserId = new Guid("deeed523-e376-4ff0-8439-06381da087ce"), CreatedTime = System.DateTime.Now };
                Blog blog3 = new Blog { Id = new Guid("97ebd8e8-e800-4cb5-9207-ec1c81399c99"), Title = "aspnetcore 3.1.2 windows hosting bundle caused 503 services unavailable", ImageUrl = "/images/blog/97ebd8e8-e800-4cb5-9207-ec1c81399c99/blog_display.jpg", UserId = new Guid("deeed523-e376-4ff0-8439-06381da087ce"), CreatedTime = System.DateTime.Now };
                Blog blog4 = new Blog { Id = new Guid("5d58229b-e8c9-490a-9666-f31bc9a8bee4"), Title = "How to get video details from YouTube with .NET Core ", ImageUrl = "/images/blog/5d58229b-e8c9-490a-9666-f31bc9a8bee4/blog_display.jpg", UserId = new Guid("deeed523-e376-4ff0-8439-06381da087ce"), CreatedTime = System.DateTime.Now };
                await _context.Blogs.AddAsync(blog1);
                await _context.Blogs.AddAsync(blog2);
                await _context.Blogs.AddAsync(blog3);
                await _context.Blogs.AddAsync(blog4);
                await _context.SaveChangesAsync();
                BlogTag blogTag1 = new BlogTag { BlogId = new Guid("1eb95ad2-6b8c-4abc-a716-8cbdb56c70fe"), TagId = new Guid("a4281f8f-8211-4721-9acf-6508e1e9fb54") };
                BlogTag blogTag2 = new BlogTag { BlogId = new Guid("44a731c0-6c0c-4b29-aee3-b3ddbb651496"), TagId = new Guid("a4281f8f-8211-4721-9acf-6508e1e9fb54") };
                BlogTag blogTag3 = new BlogTag { BlogId = new Guid("97ebd8e8-e800-4cb5-9207-ec1c81399c99"), TagId = new Guid("a4281f8f-8211-4721-9acf-6508e1e9fb54") };
                BlogTag blogTag4 = new BlogTag { BlogId = new Guid("5d58229b-e8c9-490a-9666-f31bc9a8bee4"), TagId = new Guid("a4281f8f-8211-4721-9acf-6508e1e9fb54") };
                BlogTag blogTag5 = new BlogTag { BlogId = new Guid("1eb95ad2-6b8c-4abc-a716-8cbdb56c70fe"), TagId = new Guid("ccf6a9dd-050b-4228-a92f-706ba8d28cd9") };
                BlogTag blogTag6 = new BlogTag { BlogId = new Guid("44a731c0-6c0c-4b29-aee3-b3ddbb651496"), TagId = new Guid("ccf6a9dd-050b-4228-a92f-706ba8d28cd9") };
                BlogTag blogTag7 = new BlogTag { BlogId = new Guid("97ebd8e8-e800-4cb5-9207-ec1c81399c99"), TagId = new Guid("ccf6a9dd-050b-4228-a92f-706ba8d28cd9") };
                BlogTag blogTag8 = new BlogTag { BlogId = new Guid("5d58229b-e8c9-490a-9666-f31bc9a8bee4"), TagId = new Guid("ccf6a9dd-050b-4228-a92f-706ba8d28cd9") };
                await _context.BlogTags.AddAsync(blogTag1);
                await _context.BlogTags.AddAsync(blogTag2);
                await _context.BlogTags.AddAsync(blogTag3);
                await _context.BlogTags.AddAsync(blogTag4);
                await _context.BlogTags.AddAsync(blogTag5);
                await _context.BlogTags.AddAsync(blogTag6);
                await _context.BlogTags.AddAsync(blogTag7);
                await _context.BlogTags.AddAsync(blogTag8);
                await _context.SaveChangesAsync();
                var contentHtmlTest1 = await _htmlService.OnGetHtml("https://localhost:44300/html/test1.html");
                var formatHtmlTest1 = HtmlService.ConvertHtmlToString(contentHtmlTest1);
                BlogDetail blogDetail1 = new BlogDetail { Id = new Guid("50765148-259b-4bc7-bd54-13c12931ed22"), BlogId = new Guid("1eb95ad2-6b8c-4abc-a716-8cbdb56c70fe"), HtmlContent = formatHtmlTest1 };
                var contentHtmlTest2 = await _htmlService.OnGetHtml("https://localhost:44300/html/test2.html");
                var formatHtmlTest2 = HtmlService.ConvertHtmlToString(contentHtmlTest2);
                BlogDetail blogDetail2 = new BlogDetail { Id = new Guid("e861a4b7-eef4-4e8b-a688-29223f9dacaa"), BlogId = new Guid("5d58229b-e8c9-490a-9666-f31bc9a8bee4"), HtmlContent = formatHtmlTest2 };
                var contentHtmlTest3 = await _htmlService.OnGetHtml("https://localhost:44300/html/test3.html");
                var formatHtmlTest3 = HtmlService.ConvertHtmlToString(contentHtmlTest3);
                BlogDetail blogDetail3 = new BlogDetail { Id = new Guid("65c22f86-0ab2-43dd-8825-5ff4ad1bd015"), BlogId = new Guid("97ebd8e8-e800-4cb5-9207-ec1c81399c99"), HtmlContent = formatHtmlTest3 };
                var contentHtmlTest4 = await _htmlService.OnGetHtml("https://localhost:44300/html/test4.html");
                var formatHtmlTest4 = HtmlService.ConvertHtmlToString(contentHtmlTest4);
                BlogDetail blogDetail4 = new BlogDetail { Id = new Guid("36d31b6d-ce28-4869-94a3-f4dd89cc19e9"), BlogId = new Guid("44a731c0-6c0c-4b29-aee3-b3ddbb651496"), HtmlContent = formatHtmlTest4 };
                await _context.BlogDetails.AddAsync(blogDetail1);
                await _context.BlogDetails.AddAsync(blogDetail2);
                await _context.BlogDetails.AddAsync(blogDetail3);
                await _context.BlogDetails.AddAsync(blogDetail4);
                await _context.SaveChangesAsync();
            }


        }
    }
}
