﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TechBlog.Models.EFModels;
using TechBlog.Models.ViewModels;

namespace TechBlog.Services
{
    public interface IBlogDetailService : IModelService<BlogDetailVM>
    {
        Task<BlogDetailVM> GetByBlogId(Guid id);
    }
    public class BlogDetailService : IBlogDetailService
    {
        private readonly TechBlogContext _context;
        private readonly IWebHostEnvironment _env;
        public BlogDetailService(TechBlogContext context, IWebHostEnvironment env)
        {
            _context = context;
            _env = env;
        }

        public Task<List<BlogDetailVM>> GetAll()
        {
            throw new NotImplementedException();
        }

        public async Task<BlogDetailVM> GetByBlogId(Guid id)
        {
            string webRootPath = _env.WebRootPath;
            ValueTask<Blog> blog = _context.Blogs.FindAsync(id);
            Blog blogResult = await blog;
            Task<User> user = _context.Users.Where(user => user.Id == blogResult.UserId).FirstOrDefaultAsync();
            User userResult = await user;
            Task<BlogDetail> blogDetail = _context.BlogDetails.Where(blogDetail => blogDetail.BlogId == id).FirstOrDefaultAsync();
            BlogDetail blogDetailResult = await blogDetail;
            Task<List<Tag>> tags = _context.BlogTags.Where(blogTag => blogTag.BlogId == blogResult.Id).Join(_context.Tags, blogTag => blogTag.TagId, tag => tag.Id, (blogTag, tag) => tag).ToListAsync();
            List<Tag> tagResults = await tags;

            StringWriter contentBlog = new StringWriter();
            // Decode the encoded string.
            HttpUtility.HtmlDecode(blogDetailResult.HtmlContent, contentBlog);
            BlogDetailVM blogDetailVM = new BlogDetailVM { Title = blogResult.Title, ImageUrl = blogResult.ImageUrl, Tags = tagResults, HtmlContent = contentBlog.ToString(), CreatedTime = blogResult.CreatedTime, NameAuthor = user.Result.NameDisplay, EmailAuthor = user.Result.Email, ImageAuthorUrl = userResult.ImageUrl };
            return blogDetailVM;
        }

        public Task<BlogDetailVM> GetById(Guid Id)
        {
            throw new NotImplementedException();
        }
    }
}
