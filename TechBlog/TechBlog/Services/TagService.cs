﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechBlog.Models.EFModels;

namespace TechBlog.Services
{
    public class TagService : IModelService<Tag>
    {
        private readonly TechBlogContext _context;
        public TagService(TechBlogContext context)
        {
            _context = context;
        }
        public async Task<List<Tag>> GetAll()
        {
            return await _context.Tags.ToListAsync();
        }

        public Task<Tag> GetById(Guid Id)
        {
            throw new NotImplementedException();
        }
    }
}
