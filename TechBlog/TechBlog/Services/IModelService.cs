﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TechBlog.Services
{
    public interface IModelService<T>
    {
        Task<List<T>> GetAll();
        Task<T> GetById(Guid Id);
    }
}
