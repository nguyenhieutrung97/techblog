﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechBlog.Extensions;
using TechBlog.Models.EFModels;
using TechBlog.Models.ViewModels;

namespace TechBlog.Services
{

    public enum RegisterState
    {
        Success, UserNameExist, EmailExist
    }
    public interface IAccountService
    {
        Task<User> Login(LoginVM loginVM);
        Task<RegisterState> Register(User user);
    }
    public class AccountService : IAccountService
    {

        private readonly TechBlogContext _context;
        private readonly Guid userRoleId = new Guid("daa3481e-e333-4add-aa3e-df35af99ef87");
        public AccountService(TechBlogContext context)
        {
            _context = context;
        }
        public async Task<User> Login(LoginVM loginVM)
        {
            string passwordHashed = Encryption.ComputeSha256Hash(loginVM.Password);
            Task<User> user = _context.Users.Where(user => user.Email == loginVM.Email && user.Password == passwordHashed).FirstOrDefaultAsync();
            if (await user != default)
            {
                return await user;
            }
            return null;
        }


        public async Task<RegisterState> Register(User userVM)
        {
            if (await _context.Users.AnyAsync(user => user.Email == userVM.Email) == true)
            {
                return RegisterState.UserNameExist;
            }
            else if (await _context.Users.AnyAsync(user => user.Email == userVM.Email) == true)
            {
                return RegisterState.EmailExist;
            }
            string passordHashed = Encryption.ComputeSha256Hash(userVM.Password);
            userVM.Password = passordHashed;
            userVM.ImageUrl = "/images/user/user.jpg";
            userVM.RoleId = userRoleId;
            _context.Add(userVM);
            await _context.SaveChangesAsync();
            return RegisterState.Success;
        }
    }
}
