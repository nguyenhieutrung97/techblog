﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TechBlog.Models.EFModels;
using TechBlog.Models.ViewModels;
using TechBlog.Extensions;
using System.Web;

namespace TechBlog.Services
{

    public interface IBlogService : IModelService<BlogVM>
    {
        Task<List<BlogVM>> GetByTagName(string tagName);
        Task<bool> Create(Guid userId, BlogPostVM blogPostVM);
    }
    public class BlogService : IBlogService
    {
        private readonly TechBlogContext _context;
        private readonly IWebHostEnvironment _env;
        public BlogService(TechBlogContext context, IWebHostEnvironment env)
        {
            _context = context;
            _env = env;
        }

      

        public async Task<List<BlogVM>> GetAll()
        {
            string webRootPath = _env.WebRootPath;
            Task<List<Blog>> blogs = _context.Blogs.ToListAsync();
            List<Blog> blogResults = await blogs;
            List<BlogVM> blogVMs = new List<BlogVM>();

            foreach (var blog in blogResults)
            {
                User user = _context.Users.Find(blog.UserId);
                Task<List<Tag>> tags =
                    _context.BlogTags.Where(blogTag => blogTag.BlogId == blog.Id).Join(_context.Tags, blogTag => blogTag.TagId, tag => tag.Id, (blogTag, tag) => tag).ToListAsync();
                string imageThumbnail = Path.Combine(webRootPath, blog.ImageUrl).Replace(".jpg", "-thumbnail.jpg");
                BlogVM blogVM = new BlogVM { Id = blog.Id, Title = blog.Title, ImageUrl = imageThumbnail, CreatedTime = blog.CreatedTime, ImageAuthorUrl = Path.Combine(webRootPath, user.ImageUrl), NameAuthor = user.NameDisplay, Tags = await tags };
                blogVMs.Add(blogVM);
            }
            return blogVMs;
        }

        public Task<BlogVM> GetById(Guid Id)
        {
            throw new NotImplementedException();
        }
        public async Task<List<BlogVM>> GetByTagName(string tagName)
        {
            string webRootPath = _env.WebRootPath;
            Task<List<Blog>> blogs = _context.BlogTags.Include(bt => bt.Tag).Where(bt => bt.Tag.Name == tagName).Include(bt => bt.Blog).Select(bt => new Blog { Id = bt.BlogId, Title = bt.Blog.Title, ImageUrl = bt.Blog.ImageUrl, CreatedTime = bt.Blog.CreatedTime, UserId = bt.Blog.UserId }).ToListAsync();
            List<Blog> blogResults = await blogs;
            List<BlogVM> blogVMs = new List<BlogVM>();

            foreach (var blog in blogResults)
            {
                User user = _context.Users.Find(blog.UserId);
                Task<List<Tag>> tags =
                    _context.BlogTags.Where(blogTag => blogTag.BlogId == blog.Id).Join(_context.Tags, blogTag => blogTag.TagId, tag => tag.Id, (blogTag, tag) => tag).ToListAsync();
                BlogVM blogVM = new BlogVM { Id = blog.Id, Title = blog.Title, ImageUrl = Path.Combine(webRootPath, blog.ImageUrl), CreatedTime = blog.CreatedTime, ImageAuthorUrl = user.ImageUrl, NameAuthor = user.NameDisplay, Tags = await tags };
                blogVMs.Add(blogVM);
            }
            return blogVMs;
        }
        public async Task<bool> Create(Guid userId, BlogPostVM blogPostVM)
        {

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    Blog blog = new Blog { Title = blogPostVM.Title, UserId = userId, ImageUrl = "", CreatedTime = System.DateTime.Now };
                    _context.Blogs.Add(blog);
                    _context.SaveChanges();
                    string imageUrl = await FileService.UploadSingleImage(_env.WebRootPath, blogPostVM.ImageDisplay, "blog", blog.Id.ToString(), "blog_display");
                    blog.ImageUrl = imageUrl;
                    _context.SaveChanges();
                    _context.BlogDetails.Add(new BlogDetail { BlogId = blog.Id, HtmlContent = HttpUtility.HtmlEncode(blogPostVM.HtmlContent) });
                    _context.SaveChanges();
                    foreach(string tagId in blogPostVM.Tags)
                    {
                        _context.Add(new BlogTag { BlogId = blog.Id, TagId = new Guid(tagId) });
                    }
                    _context.SaveChanges();
                    string pathImageRoot = _env.WebRootPath+blog.ImageUrl.Replace("/","\\");
                    FileService.ResizeImage(pathImageRoot, pathImageRoot, 825, 347);
                    FileService.ResizeImage(pathImageRoot, pathImageRoot.Replace(".jpg", "-thumbnail.jpg"), 400, 160);
                    // Commit transaction if all commands succeed, transaction will auto-rollback
                    // when disposed if either commands fails
                    transaction.Commit();
                }
                catch (Exception)
                {
                    return false;
                }
                
            }
            return true;
        }
    }
}
