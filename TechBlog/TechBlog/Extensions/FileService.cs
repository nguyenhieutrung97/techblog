﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using SixLabors.ImageSharp;
using SixLabors.ImageSharp.Processing;
using SixLabors.ImageSharp.PixelFormats;

namespace TechBlog.Extensions
{
    public static class FileService
    {
        public static async Task<string> UploadSingleImage(string webRootPath, IFormFile imageData, string typeObject, string objectId, string nameFile)
        {
            string fileStoreImage = "", filePath = "";
            // Saving Image on Server
            if (imageData.Length > 0)
            {
                fileStoreImage = Path.GetFullPath(webRootPath + "\\images\\" + typeObject + "\\" + objectId);
                Directory.CreateDirectory(fileStoreImage);
                filePath = Path.Combine(fileStoreImage, nameFile + ".jpg");
                using (var stream = System.IO.File.Create(filePath))
                {
                    await imageData.CopyToAsync(stream);
                }
            }
            //return filePath;
            string urlHost = "/images/" + typeObject + "/" + objectId.ToString() + "/" + nameFile + ".jpg";
            return urlHost;
        }
        public static void ResizeImage(string inputPath, string outputPath, int width, int height)
        {
            // Image.Load(string path) is a shortcut for our default type. 
            // Other pixel formats use Image.Load<TPixel>(string path))
            using (Image image = Image.Load(inputPath))
            {
                image.Mutate(x => x
                     .Resize(image.Width > width ? width : image.Width, image.Height > height ? height : image.Height) );
                image.Save(outputPath); // Automatic encoder selected based on extension.
            }
        }

    }
}
