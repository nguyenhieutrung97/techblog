﻿using Microsoft.AspNetCore.Html;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace TechBlog.Extensions
{
    public interface IHtmlService
    {
        Task<string> OnGetHtml(string url);
    }
    public class HtmlService : IHtmlService
    {
        private readonly IHttpClientFactory _clientFactory;
        public HtmlService(IHttpClientFactory clientFactory)
        {
            _clientFactory = clientFactory;
        }
        public async Task<string> OnGetHtml(string url)
        {
            var request = new HttpRequestMessage(HttpMethod.Get,
           url);

            var client = _clientFactory.CreateClient();

            var response = await client.SendAsync(request);

            if (response.IsSuccessStatusCode)
            {
                return await response.Content.ReadAsStringAsync();
            }
            else
            {
                return "";
            }
        }
        public static string ConvertHtmlToString(string content)
        {
            return HttpUtility.HtmlEncode(content);
        }
    }
}
