﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TechBlog.Models.EFModels;
using TechBlog.Services;

namespace TechBlog.Components
{
    public class TagMenu : ViewComponent
    {
        private readonly IModelService<Tag> _tagService;
        public TagMenu(IModelService<Tag> tagService)
        {
            _tagService = tagService;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View(await _tagService.GetAll());
        }
    }
}
